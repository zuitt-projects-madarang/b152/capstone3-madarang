import {useState,useEffect} from 'react' 

import {Container} from 'react-bootstrap'
import {BrowserRouter as Router} from 'react-router-dom'
import {Route,Routes} from 'react-router-dom'

import AppNavbar from './components/AppNavbar'

import Home from './pages/Home'
import Login from './pages/Login'
import Register from './pages/Register'
import Logout from './pages/Logout'
import Products from './pages/Products'
import ViewProduct from './pages/ViewProduct'
import ErrorPage from './pages/ErrorPage'

import {UserProvider} from './userContext'

export default function App(){
  const [user,setUser] = useState({
    id: null,
    isAdmin: null
  })

  useEffect(()=>{
    fetch('https://fierce-basin-91082.herokuapp.com/users/details',{
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      setUser({
        id: data._id,
        isAdmin: data.isAdmin
      })
    })
  },[])

  const unsetUser = () => {
    localStorage.clear()
  }

  return (
    <>
      <UserProvider value={{user,setUser,unsetUser}}>
        <Router>
          <AppNavbar />
          <Container>
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/login" element={<Login />} />
              <Route path="/register" element={<Register />} />
              <Route path="/logout" element={<Logout />} />
              <Route path="/store" element={<Products />} />
              <Route path="/store/viewProduct/:productId" element={<ViewProduct />} />
              <Route path="*" element={<ErrorPage />} />
            </Routes>
          </Container>
        </Router>
      </UserProvider>
    </>
  )
}