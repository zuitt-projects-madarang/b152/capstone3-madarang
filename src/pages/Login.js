import {useState,useContext,useEffect} from 'react' 
import {Form,Button} from 'react-bootstrap'
import Swal from 'sweetalert2'
import UserContext from '../userContext'
import {Navigate} from 'react-router-dom'

export default function Login(){

	const {user,setUser} = useContext(UserContext)

	const [email,setEmail] = useState("")
	const [password,setPassword] = useState("")

	const [isActive,setIsActive] = useState(false)

	useEffect(()=>{
		if(email !== "" && password !== ""){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	},[email,password])

	function loginUser(e){
		e.preventDefault()
		fetch('https://fierce-basin-91082.herokuapp.com/users/login',{
			method: 'POST',
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data.accessToken){
				Swal.fire({
					icon: "success",
					title: "Welcome!",
					text: "Thank you for logging in!"
				})
				localStorage.setItem('token',data.accessToken)

				fetch('https://fierce-basin-91082.herokuapp.com/users/details',{
					method: 'GET',
					headers: {
						"Authorization": `Bearer ${localStorage.getItem('token')}`
					}
				})
				.then(res => res.json())
				.then(data => {
					setUser({
						id: data._id,
						isAdmin: data.isAdmin
					})
				})
			} else {
				Swal.fire({
					icon: "error",
					title: "Login Failed",
					text: data.message
				})
			}
		})
	}

	return (
		user.id
		?
		<Navigate to="/" replace={true} />
		:
		<>
			<h1 className="my-5 text-center">Login</h1>
			<Form onSubmit={e => loginUser(e)} className="text-center">
				<Form.Group className="text-center">
					<Form.Label className="mt-3">Email:</Form.Label>
					<Form.Control type="email" placeholder="Enter email" required value={email} onChange={e => {setEmail(e.target.value)}} className="w-50 mx-auto text-center" />
				</Form.Group>
				<Form.Group className="text-center">
					<Form.Label className="mt-3">Password:</Form.Label>
					<Form.Control type="password" placeholder="Enter password" required value={password} onChange={e => {setPassword(e.target.value)}} className="w-50 mx-auto text-center" />
				</Form.Group>
				{
					isActive
					? <Button variant="success" type="submit" className="my-5">Login</Button>
					: <Button variant="danger" disabled className="my-5">Login</Button>
				}
			</Form>
			<p className="text-center">Don't have an account yet? <a href="/register" className="link-danger">Register</a> here.</p>
		</>	
	)
}