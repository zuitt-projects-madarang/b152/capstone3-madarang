import {useState,useEffect,useContext} from 'react'
import {Card,Row,Col} from 'react-bootstrap'
import {useParams} from 'react-router-dom'
import UserContext from '../userContext'
import {Link} from 'react-router-dom'

export default function ViewProduct(){

	const {productId} = useParams()
	const {user} = useContext(UserContext)
	console.log(user)

	const [productDetails,setProductDetails] = useState({
		name: null,
		description: null,
		price: null
	})

	useEffect(() => {
		fetch(`https://fierce-basin-91082.herokuapp.com/products/getSingleProduct/${productId}`)
		.then(res => res.json())
		.then(data => {
			setProductDetails({
				name: data.name,
				description: data.description,
				price: data.price
			})
		})
	},[productId])

	return (
		<Row className="mt-5">
			<Col xs={12} md={6} className="mx-auto">
				<Card className="bg-dark">
					<Card.Body className="text-center">
						<Card.Title><h1 className="text-info">{productDetails.name}</h1></Card.Title>
						<Card.Subtitle><h4 className="text-light">Description:</h4></Card.Subtitle>
						<Card.Text className="text-light">{productDetails.description}</Card.Text>
						<Card.Subtitle><h4 className="text-light">Price:</h4></Card.Subtitle>
						<Card.Text className="text-warning fw-bold">{productDetails.price} G</Card.Text>
						<Link to="/store" className="btn btn-secondary">Back to Store</Link>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	)
}