import {useState,useContext,useEffect} from 'react' 
import {Form,Button} from 'react-bootstrap'
import Swal from 'sweetalert2'
import UserContext from '../userContext'
import {Navigate} from 'react-router-dom'

export default function Register(){

	const {user} = useContext(UserContext)

	const [firstName,setFirstName] = useState("")
	const [lastName,setLastName] = useState("")
	const [email,setEmail] = useState("")
	const [mobileNo,setMobileNo] = useState("")
	const [password,setPassword] = useState("")
	const [confirmPassword,setConfirmPassword] = useState("")

	const [isActive,setIsActive] = useState(false)

	useEffect(()=>{
		if(firstName !== "" && lastName !== "" && email !== "" && mobileNo !== "" && password !== "" && confirmPassword !== "" &&
		   mobileNo.length === 11 &&
		   password === confirmPassword){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	},[firstName,lastName,email,mobileNo,password,confirmPassword])

	function registerUser(e){
		e.preventDefault()
		fetch('https://fierce-basin-91082.herokuapp.com/users',{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data.email){
				Swal.fire({
					icon: "success",
					title: "Success!",
					text: "Thank you for registering!"
				})
				window.location.href = "/login"
			} else {
				Swal.fire({
					icon: "error",
					title: "Failed.",
					text: "Something went wrong."
				})
			}
		})
	}

	return (
		user.id
		?
		<Navigate to="/" replace={true} />
		:
		<>
			<h1 className="my-5 text-center">Register</h1>
			<Form onSubmit={e => registerUser(e)} className="text-center">
				<Form.Group>
					<Form.Label>First Name</Form.Label>
					<Form.Control type="text" placeholder="Enter First Name" required value={firstName} onChange={e => {setFirstName(e.target.value)}} className="w-50 mx-auto text-center" />
				</Form.Group>
				<Form.Group>
					<Form.Label className="mt-3">Last Name</Form.Label>
					<Form.Control type="text" placeholder="Enter Last Name" required value={lastName} onChange={e => {setLastName(e.target.value)}} className="w-50 mx-auto text-center" />
				</Form.Group>
				<Form.Group>
					<Form.Label className="mt-3">Email</Form.Label>
					<Form.Control type="email" placeholder="Enter Email" required value={email} onChange={e => {setEmail(e.target.value)}} className="w-50 mx-auto text-center" />
				</Form.Group>
				<Form.Group>
					<Form.Label className="mt-3">Mobile No.</Form.Label>
					<Form.Control type="number" placeholder="Enter Mobile No." required value={mobileNo} onChange={e => {setMobileNo(e.target.value)}} className="w-50 mx-auto text-center" />
				</Form.Group>
				<Form.Group>
					<Form.Label className="mt-3">Password</Form.Label>
					<Form.Control type="password" placeholder="Enter Password" required value={password} onChange={e => {setPassword(e.target.value)}} className="w-50 mx-auto text-center" />
				</Form.Group>
				<Form.Group>
					<Form.Label className="mt-3">Confirm Password</Form.Label>
					<Form.Control type="password" placeholder="Confirm Password" required value={confirmPassword} onChange={e => {setConfirmPassword(e.target.value)}} className="w-50 mx-auto text-center" />
				</Form.Group>
				{
					isActive
					? <Button variant="success" type="submit" className="my-5">Register</Button>
					: <Button variant="danger" disabled className="my-5">Register</Button>
				}
			</Form>
			<p className="text-center">Already have an account? <a href="/login" className="link-danger">Login</a> here.</p>
		</>
	)
}