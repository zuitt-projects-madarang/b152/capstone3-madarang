import {useState,useEffect,useContext} from 'react' 
import Banner from '../components/Banner'
import Banner2 from '../components/Banner2'
import ProductCard from '../components/Product'
import AddProduct from '../components/AddProduct'
import AdminDashboard from '../components/AdminDashboard' 

import UserContext from '../userContext'

export default function Products(){

	const {user} = useContext(UserContext)

	let productBanner = {
		title: "Welcome to the Store.",
		description: "Browse our catalogue.",
		buttonText: "Register/Login to Purchase.",
		destination: "/register"
	}
	let productBanner2 = {
		title: "Welcome to the Store.",
		description: "Browse our catalogue.",
	}

	const [productsArray,setProductsArray] = useState([])

	useEffect(()=>{
		fetch('https://fierce-basin-91082.herokuapp.com/products/active')
		.then(res => res.json())
		.then(data => {
			setProductsArray(data.map(product => {
				return (
					<ProductCard key={product._id} productProp={product} />
				)
			}))
		})
	},[])

	return(
		<>
			{
				user.isAdmin 
				?
				<>
					<AdminDashboard />
					<AddProduct />
				</>
				:
				user.id
				?
				<>
					<Banner2 bannerProp2={productBanner2} />
					{productsArray}
				</>
				:
				<>
					<Banner bannerProp={productBanner} />
					{productsArray}
				</>			
			}	
		</>
	)
}