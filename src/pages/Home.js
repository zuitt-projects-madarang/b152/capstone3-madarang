import Banner from '../components/Banner'
import Showcase from '../components/Carousel'
import Highlights from '../components/Highlight'

export default function Home(){
	let bannerData = {
		title: "Potion's 11",
		description: "It's raining meds! Hallelujah!",
		buttonText: "View Selection",
		destination: "/store"
	}
	return(
		<>
			<Banner bannerProp={bannerData} />
			<Showcase />
			<Highlights />
		</>
	)
}