import Banner from '../components/Banner'

export default function ErrorPage(){
	let errorBanner = {
		title: "Page Not Found.",
		description: "Page does not exist.",
		buttonText: "Back to Home",
		destination: "/"
	}
	return <Banner bannerProp={errorBanner}/>
}