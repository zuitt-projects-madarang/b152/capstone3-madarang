import {useState,useEffect,useContext} from 'react' 
import UserContext from '../userContext'
import{Table,Button} from 'react-bootstrap'

export default function AdminDashboard(){

	const {user} = useContext(UserContext)

	const [allProducts,setAllProducts] = useState([])

	function archive(productId){
		fetch(`https://fierce-basin-91082.herokuapp.com/products/archive/${productId}`,{
			method: 'PUT',
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			window.location.href = "/store"
		})
	}

	function activate(productId){
		fetch(`https://fierce-basin-91082.herokuapp.com/products/activate/${productId}`,{
			method: 'PUT',
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			window.location.href = "/store"
		})
	}

	useEffect(() => {
		if(user.isAdmin){
			fetch('https://fierce-basin-91082.herokuapp.com/products/allProducts',{
				headers: {
					'Authorization': `Bearer ${localStorage.getItem('token')}`
				}
			})
			.then(res => res.json())
			.then(data => {
				setAllProducts(data.map(product => {
					return (
						<tr key={product._id}>
							<td>{product._id}</td>
							<td>{product.name}</td>
							<td>{product.price}</td>
							<td>{product.isActive ? "Active" : "Inactive"}</td>
							<td>
								{
									product.isActive
									? 
									<Button variant="danger" className="mx-2" onClick={()=>{archive(product._id)}}>Archive</Button>
									:
									<Button variant="success" className="mx-2" onClick={()=>{activate(product._id)}}>Activate</Button>
								}
							</td>
						</tr>
					)
				}))
			})
		}
	})

	return(
		<>
			<h1 className="my-5 text-center">Admin Dashboard</h1>
			<Table striped bordered hover>
				<thead className="text-center">
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Price</th>
						<th>Status</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody className="text-center">
					{allProducts}
				</tbody>
			</Table>
		</>
	)
}