import {Row,Col} from 'react-bootstrap'
import homemade from '../resources/homemade.png'
import ingredients from '../resources/ingredients.png'
import guarantee from '../resources/guarantee.png'

export default function Highlights(){
	return(
		<Row className="my-5">
			<Col xs={12} md={4} className="text-center p-3">
					<img
					alt="ingredients"
					src={ingredients}
					width="200"
					height="200"
					className="d-inlink-block mx-2 mb-5"
					/>
					<h2 className="p-3 text-danger">All Natural</h2>
				<p>
					Ingredients used for our products are gathered fresh from Mother Nature. So enjoy a healthier, tastier and cost-efficient brew to spice up your life. 
				</p>
			</Col>
			<Col xs={12} md={4} className="text-center p-3">
					<img
					alt="homemande"
					src={homemade}
					width="200"
					height="200"
					className="d-inlink-block mx-2 mb-5"
					/>
					<h2 className="p-3 text-danger">Homemade</h2>
				<p>
					Products found in our store are not mass-produced and can only be found here. Have a taste of our home-brewed selection that will leave you wanting more.
				</p>
			</Col>
			<Col xs={12} md={4} className="text-center p-3">
					<img
					alt="guarantee"
					src={guarantee}
					width="200"
					height="200"
					className="d-inlink-block mx-2 mb-5"
					/>
					<h2 className="p-3 text-danger">100% Money-Back</h2>
				<p>
					Your satisfaction is what's most important to us. If you are not satisfied with our products, we ensure you that 100% of your payment will be refunded.
				</p>
			</Col>
		</Row>
	)
}