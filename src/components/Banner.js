import {Row, Col} from 'react-bootstrap' 

export default function Banner({bannerProp}){
	return(
		<Row className="justify-content-md-center mt-4">
			<Col className="p-4 my-0" md="auto">
				<h1 className="mb-3 text-center">{bannerProp.title}</h1>
				<p className="my-3 text-center">{bannerProp.description}</p>
				<div className="text-center my-3">
					<a href={bannerProp.destination} className="btn btn-danger">{bannerProp.buttonText}</a>
				</div>
			</Col>
		</Row>
	)
}