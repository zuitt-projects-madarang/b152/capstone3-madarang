import {useContext} from 'react'
import {Nav,Navbar,Container} from 'react-bootstrap'
import potion2 from '../resources/potion2.png'
import shop from '../resources/shop.png'
import register from '../resources/register.png'
import UserContext from '../userContext'
import {Link} from 'react-router-dom'

export default function AppNavbar(){
	const {user} = useContext(UserContext)
	return(
		<Navbar bg="dark" variant="dark" sticky="top" expand="lg">
			<Container fluid>
				<Navbar.Brand href="/">
				<img
					alt="potion"
					src={potion2}
					width="30"
					height="30"
					className="d-inlink-block align-top mx-2"
				/>
				<em>Potion's 11</em>
				</Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav" />
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="me-auto">
						<Link to="/store" className="nav-link">
						<img
							alt="shop"
							src={shop}
							width="25"
							height="25"
							className="d-inlink-block mx-2"
						/>
						Store
						</Link>
					</Nav>
					<Nav className="me-3">
						{
							user.id 
							?
							<Link to="/logout" className="nav-link">Logout</Link>
							:
							<>
								<Link to="/register" className="nav-link">Register</Link>
								<Link to="/login" className="nav-link">
								Login
								{' '}
								<img
									alt="user"
									src={register}
									width="25"
									height="25"
									className="d-inlink-block mx-2"
								/>
								</Link>
							</>
						}
					</Nav>
				</Navbar.Collapse>
			</Container>
		</Navbar>
	)
}