import {Row, Col} from 'react-bootstrap' 

export default function Banner2({bannerProp2}){
	return(
		<Row className="justify-content-md-center">
			<Col className="p-4 my-0" md="auto">
				<h1 className="mb-3 text-center">{bannerProp2.title}</h1>
				<p className="my-3 text-center">{bannerProp2.description}</p>
			</Col>
		</Row>
	)
}