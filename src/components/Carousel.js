import Carousel from 'react-bootstrap/Carousel'
import firstCarousel from '../resources/firstCarousel.jpg'
import secondCarousel from '../resources/secondCarousel.png'
import thirdCarousel from '../resources/thirdCarousel.jpg'

export default function Showcase(){
	return (
		<Carousel fade className="my-4">
		  <Carousel.Item>
		    <img
		      className="d-block w-75 mx-auto"
		      src={firstCarousel}
		      alt="First slide"
		    />
		  </Carousel.Item>
		  <Carousel.Item>
		    <img
		      className="d-block w-75 mx-auto"
		      src={secondCarousel}
		      alt="Second slide"
		    />
		  </Carousel.Item>
		  <Carousel.Item>
		    <img
		      className="d-block w-75 mx-auto"
		      src={thirdCarousel}
		      alt="Third slide"
		    />
		  </Carousel.Item>
		</Carousel>
	)	
}