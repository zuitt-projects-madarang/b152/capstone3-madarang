import Modal from 'react-bootstrap/Modal'
import {Button,Form} from 'react-bootstrap'
import {useState} from 'react'
import Swal from 'sweetalert2'

export default function AddProduct(){
	const [show, setShow] = useState(false);
	const [name,setName] = useState("")
	const [description,setDescription] = useState("")
	const [price,setPrice] = useState("")

	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

	function createProduct(e){
		e.preventDefault()
		
		fetch('https://fierce-basin-91082.herokuapp.com/products/',{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data._id){
				Swal.fire({
					icon: "success",
					title: "Product Added to Store!",
				})
				window.location.href = '/store'
			} else {
				Swal.fire({
					icon: "error",
					title: "Failed.",
					text: data.message
				})
			}
		})
	}

	return (
	    <>
	      <div className="text-center">
		      <Button variant="warning" onClick={handleShow} className="my-5">
		        Add Product
		      </Button>
	      </div>

	      <Modal show={show} onHide={handleClose}>
	        <h1 className="my-5 text-center">Add Product</h1>
	        <Form className="mx-3 text-center" onSubmit={e => createProduct(e)}>
	        	<Form.Group>
	        		<Form.Label>Name:</Form.Label>
	        		<Form.Control type="text" placeholder="Enter Product Name" required value={name} onChange={e => {setName(e.target.value)}} className="w-75 mx-auto" />
	        	</Form.Group>
	        	<Form.Group>
	        		<Form.Label>Description:</Form.Label>
	        		<Form.Control type="text" placeholder="Enter Product Description" required value={description} onChange={e => {setDescription(e.target.value)}} className="w-75 mx-auto" />
	        	</Form.Group>
	        	<Form.Group>
	        		<Form.Label>Price:</Form.Label>
	        		<Form.Control type="number" placeholder="Enter Price" required value={price} onChange={e => {setPrice(e.target.value)}} className="w-75 mx-auto" />
	        	</Form.Group>
	        	<Button variant="warning" type="submit" className="my-5">Create</Button>
	        </Form>
	      </Modal>
	    </>
	)
}