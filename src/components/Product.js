import {Card,Row,Col} from 'react-bootstrap'
import {Link} from 'react-router-dom'

export default function ProductCard({productProp}){
	return (
		<Row>
			<Col xs={12} md={5} className="mx-auto">
				<Card className="my-4 bg-dark">
					<Card.Body>
						<Card.Title className="text-center text-light fw-bold">
							<u>{productProp.name}</u>
						</Card.Title>
						<Card.Text className="text-center text-light">
							{productProp.description}
						</Card.Text>
						<Card.Text className="text-center text-warning fw-bold">
							{productProp.price} G
						</Card.Text>
						<div className="text-center">
							<Link to={`/store/viewProduct/${productProp._id}`} className="btn btn-success">View Product</Link>
						</div>
					</Card.Body>
				</Card>
			</Col>
		</Row>		
	)
}